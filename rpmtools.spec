Name:     rpmtools
Version:  1.0
Release:  8%{?dist}
Summary:  Local RPM toolkit
BuildArch: noarch

Group:    Development/System
License:  None
URL:      http://localhost
Source0:  http://localhost/rpmtools-%{version}.tar.gz

Requires: fedora-packager

%description
Local RPM tools

%prep
tar cvzf rpmtools-%{version}.tar.gz scripts --exclude-vcs --transform='s|^scripts/|rpmtools-%{version}/|'
md5sum rpmtools-%{version}.tar.gz > sources
%setup -q

%build

%install
mkdir -p %{buildroot}/%{_bindir}
install -m 755 * %{buildroot}/%{_bindir}

%files
%{_bindir}/*

%changelog
* Wed Apr 03 2013 Hercinger Viktor <hercinger.viktor@gmail.com> - 1.0-8
- try to deduce the version automatically

* Tue Apr 02 2013 Hercinger Viktor <hercinger.viktor@gmail.com> - 1.0-7
- added archive source creator

* Fri Mar 29 2013 Hercinger Viktor <hercinger.viktor@gmail.com> - 1.0-6
- do not clean up repo before rebuilding with new packages

* Tue Mar 19 2013 Hercinger Viktor <hercinger.viktor@gmail.com> - 1.0-5
- fixed checkout bug

* Mon Mar 18 2013 Hercinger Viktor <hercinger.viktor@gmail.com> - 1.0-4
- changed default spec path
- can specify custom spec path

* Fri Mar 15 2013 Hercinger Viktor <hercinger.viktor@gmail.com> - 1.0-3
- added branch change option to rpmdev-build-devspec

* Wed Mar 13 2013 Hercinger Viktor <hercinger.viktor@gmail.com> - 1.0-2
- added notification to rpmdev-build-devspec
- added proxy call to rpmdev-make-repo in rpmdev-build-devspec

* Thu Feb 21 2013 Viktor Hercinger <vhercing@redhat.com> - 1.0.0-1
- build-spec added
- make-repo added
